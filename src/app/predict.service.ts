import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  private url = "https://dtlycq6mm8.execute-api.us-east-1.amazonaws.com/test";

  predict(mathGrade:number,psyGrade:number,paid:boolean){
    let json = {
      'data':{
        "mathGrade":mathGrade,
        "psyGrade":psyGrade,
        "paid":paid
      }
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res =>{ 
        console.log(res);
        let final = res.body;
        console.log(final);
        final = final.replace('[','');
        final = final.replace(']','');
        return final;
      })
    ) 
  }

  constructor(private http:HttpClient) { }
}
