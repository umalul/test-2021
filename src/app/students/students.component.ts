import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PredictService } from '../predict.service';
import { StudentsService } from '../students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  userId;
  students:Student[];
  students$;
  displayedColumns: string[] = ['user', 'mathAveGrade', 'psiAveGrade','paid', 'Delete', 'res'];
  
  deleteCustomer(index){
    let id = this.students[index].id;
    this.studentsService.deleteStudent(id);
  }



 
  constructor(private studentsService:StudentsService,
    private authService:AuthService, private predictService:PredictService ) { }

  ngOnInit(): void { 
          this.students$ = this.studentsService.getStudent();
          this.students$.subscribe(
            docs => {         
              this.students = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const student:Student = document.payload.doc.data();
                student.id = document.payload.doc.id;
                   this.students.push(student); 
              }                        
            }
          )
      
  }   
}
