import { Student } from './interfaces/student';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  studentCollection:AngularFirestoreCollection = this.db.collection('students');
  
  
  getStudent(): Observable<any[]> {
    this.studentCollection = this.db.collection(`students`, 
       ref => ref.limit(10))
    return this.studentCollection.snapshotChanges();    
  } 
  
  deleteStudent(id:string){
    this.db.doc(`students/${id}`).delete();
  }
  
  addStudent(user:string, mathAveGrade:number,psiAveGrade:number,dropRes:string,paid:boolean){
    const student:Student = {user:user, mathAveGrade:mathAveGrade, psiAveGrade:psiAveGrade,dropRes:dropRes,paid:paid}
    this.studentCollection.add(student);
  } 
 
    
  
  
  constructor(private db: AngularFirestore) { }
}
