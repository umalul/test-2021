import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';
import { PredictService } from '../predict.service';


@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  mathAveGrade:number;
  psiAveGrade:number;
  paid:boolean;
  dropRes:string;
  email;
  predict(mathAveGrade,psiAveGrade,paid){
    this.pred.predict(mathAveGrade,psiAveGrade,paid).subscribe(
      res => {
        if (res > 0.5){
        var drop = "Will not drop"
      }
        else{
          var drop = "Will drop"
        }
      this.dropRes = drop
      }
    );
  }

  constructor(private pred:PredictService,private students:StudentsService,private router:Router, private auth:AuthService) { }
  onSubmit(){
  this.students.addStudent(this.email, this.mathAveGrade, this.psiAveGrade,  this.dropRes, this.paid);
  this.router.navigate(['/students'])
  }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      res=>{
        this.email = res.email
      }
    )
  }

}
