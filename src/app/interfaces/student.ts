export interface Student {
    id?: string;
    mathAveGrade:number;
    psiAveGrade:number;
    paid:boolean;
    dropRes:string;
    user:string;
}
